<?php
require_once('character.php');
require_once('hero.php');
/*
// get user properties
$hero_name       = filter_var($_POST['hero_name'],      FILTER_SANITIZE_STRING);
$hero_health     = filter_var($_POST['hero_health'],    FILTER_VALIDATE_INT, 0);
$hero_strength   = filter_var($_POST['hero_strength'],  FILTER_VALIDATE_INT, 0);
$hero_defence    = filter_var($_POST['hero_defence'],   FILTER_VALIDATE_INT, 0);
$hero_speed      = filter_var($_POST['hero_speed'],     FILTER_VALIDATE_INT, 0);
$hero_luck       = filter_var($_POST['hero_luck'],      FILTER_VALIDATE_INT, 0);

// get hero skills
$hero_strike_twice  = filter_var($_POST['hero_strike_twice'],   FILTER_VALIDATE_INT, 0);
$hero_magic_shield  = filter_var($_POST['hero_magic_shield'],   FILTER_VALIDATE_INT, 0);

// get beast properties
$beast_name      = filter_var($_POST['beast_name'],      FILTER_SANITIZE_STRING);
$beast_health    = filter_var($_POST['beast_health'],    FILTER_VALIDATE_INT, 0);
$beast_strength  = filter_var($_POST['beast_strength'],  FILTER_VALIDATE_INT, 0);
$beast_defence   = filter_var($_POST['beast_defence'],   FILTER_VALIDATE_INT, 0);
$beast_speed     = filter_var($_POST['beast_speed'],     FILTER_VALIDATE_INT, 0);
$beast_luck      = filter_var($_POST['beast_luck'],      FILTER_VALIDATE_INT, 0);
*/
// max rounds
$max_rounds_no   = 20;


// init hero
$hero = new Hero($hero_name,
                $hero_health,
                $hero_strength,
                $hero_defence,
                $hero_speed,
                $hero_luck,
                $hero_strike_twice,
                $hero_magic_shield);
// init beast
$beast = new Character($beast_name,
                        $beast_health,
                        $beast_strength,
                        $beast_defence,
                        $beast_speed,
                        $beast_luck);

// decide who begins
$hero_s_turn = true;
if($beast_speed > $hero_speed || ( $beast_speed == $hero_speed && $beast_luck > $hero_luck )) {
    $hero_s_turn = false;
}

// start battle
$turns      = 0;
$attacker   = null;
$defender   = null;
$hit_damage = 0;
$damage     = 0;
while(battle_on_the_role()) {
    if($hero_s_turn) {
        $attacker = &$hero;
        $defender = $beast;
    } else {
        $attacker = &$beast;
        $defender = $hero;
    }
    $hit_damage = $attacker->strike();
    $damage = $defender->take_hit($hit_damage);

    tell_the_story();

    // now switch turns
    $hero_s_turn = !$hero_s_turn;
    $turns++;
}

declare_winner();

function battle_on_the_role() {
    global $attacker, $defender, $max_rounds_no, $turns;
    if($turns%2 == 0 && $turns/2 >= $max_rounds_no) return false;
    if(!$attacker || !$defender) return true; // battle did not begin yet
    if($attacker->isDead()) return false;
    if($defender->isDead()) return false;
    return true;
}

function tell_the_story() {
    global $attacker, $defender, $hit_damage, $damage, $turns;
    if($turns%2 == 0) {
        echo "<h3>";echo "Round ".getRound().":</h3>";
    } else {
        echo "<br/><br/>";
    }
    echo $attacker->getName()." hit ".$hit_damage;
    echo "<br/>";
    foreach($attacker->getStrikeActions() as $action) {
        echo $attacker->getName()." ".$action;
        echo "<br/>";
    }
    foreach($defender->getDefenceActions() as $defence) {
        echo $defender->getName()." ".$defence;
        echo "<br/>";
    }
    echo $defender->getName()." got ".$damage." damage and has ".$defender->getHealth()." health left";
}

function getRound() {
    global $turns;
    return floor($turns/2)+1;
}

function declare_winner() {
    global $attacker, $defender;
    echo "<h3>And the winner is ".$attacker->getName()." !</h3>";
}

?>