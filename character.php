<?php

class Character
{
    protected $healh      = 0;
    protected $strength   = 0;
    protected $defence    = 0;
    protected $speed      = 0;
    protected $luck       = 0;
    protected $name       = 'character';

    protected $luck_built       = 0;
    protected $strike_actions   = array();
    protected $defence_actions  = array();

    function __construct(
        $name,
        $health,
        $strength,
        $defence,
        $speed,
        $luck)
    {
        $this->name     = $name;
        $this->health   = $health;
        $this->strength = $strength;
        $this->defence  = $defence;
        $this->speed    = $speed;
        $this->luck     = $luck;
    }

    public function strike()
    {
        $this->strike_actions = array();
        return $this->strength;
    }

    public function take_hit($hit_damage)
    {
        $this->defence_actions = array();
        $hit_damage = $this->defence($hit_damage);
        $this->luck_built++;

        if($this->luck_built > 0 && rand(0,1)) {
            $hit_damage = 0;
            $this->defence_actions[] = 'gets lucky';
            // stretch next luck
            $this->luck_built = 0 - ( 100/$this->luck - $this->luck_built );
        }

        if($this->luck_built >= 100/$this->luck) {
            // reset counter on each set of hits per luck
            $this->luck_built = 0;
        }

        $this->health -= $hit_damage;
        if($this->health < 0) {
            $this->health = 0;
        }
        return $hit_damage;
    }

    /**
     * Offer the character a chance to defense himself
     *
     * @param   int     $hit_damage The damage that attacker applies
     * @return  int     the hit damage after the character applies defenses
     *
     */
    public function defence($hit_damage)
    {
        $hit_damage = $hit_damage - $this->defence;
        if($hit_damage < 0) {
            $hit_damage = 0;
        }
        $hit_damage = $this->extra_defence($hit_damage);
        return $hit_damage;
    }

    public function extra_defence($hit_damage)
    {
        return $hit_damage;
    }

    public function isAlive() {
        return $this->health > 0;
    }

    public function isDead() {
        return !$this->isAlive();
    }

    public function getName() {
        return $this->name;
    }

    public function getHealth() {
        return $this->health;
    }

    public function getStrikeActions() {
        return $this->strike_actions;
    }

    public function getDefenceActions() {
        return $this->defence_actions;
    }
}

?>