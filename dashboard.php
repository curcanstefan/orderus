<!DOCTYPE html>
<html lang="en">
<head>
    <title>Orderus</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet"/>
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <h2>Battle of Orderus</h2>
        <h3>
            In the ever-green forests, the great hero Orderus encounters some wild beasts.
            With hundreds of years of experience, Orderus usually knocks them out. But let's see what he does this time.
        </h3>
        
        <?php
        // get user properties
        $hero_name       = 'Orderus';
        $hero_health     = rand(70, 100);
        $hero_strength   = rand(70, 80);
        $hero_defence    = rand(45, 55);
        $hero_speed      = rand(40, 50);
        $hero_luck       = rand(10, 30);
        
        // get hero skills
        $hero_strike_twice  = 10;
        $hero_magic_shield  = 20;
        
        // get beast properties
        $beast_name      = 'The Beast';
        $beast_health    = rand(60, 90);
        $beast_strength  = rand(60, 90);
        $beast_defence   = rand(40, 60);
        $beast_speed     = rand(40, 60);
        $beast_luck      = rand(25, 45);
        ?>

        <form class="form-horizontal" role="form" method="post" action="battle.php">

            <h3>Our hero now has the following properties:</h3>
            <div class="form-group">
                <label for="hero_name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_name" name="hero_name" value="<?php echo $hero_name; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_health" class="col-sm-2 control-label">Health</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_health" name="hero_health" value="<?php echo $hero_health; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_strength" class="col-sm-2 control-label">Strength</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_strength" name="hero_strength" value="<?php echo $hero_strength; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_defence" class="col-sm-2 control-label">Defence</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_defence" name="hero_defence" value="<?php echo $hero_defence; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_speed" class="col-sm-2 control-label">Speed</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_speed" name="hero_speed" value="<?php echo $hero_speed; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_luck" class="col-sm-2 control-label">Luck</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_luck" name="hero_luck" value="<?php echo $hero_luck; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_strike_twice" class="col-sm-2 control-label">Strike Twice</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_strike_twice" name="hero_strike_twice" value="<?php echo $hero_strike_twice; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="hero_magic_shield" class="col-sm-2 control-label">Magic Shield</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="hero_magic_shield" name="hero_magic_shield" value="<?php echo $hero_magic_shield; ?>">
                </div>
            </div>


            <h3>The Beast he just met has the following properties:</h3>
            <div class="form-group">
                <label for="beast_name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_name" name="beast_name" value="<?php echo $beast_name; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="beast_health" class="col-sm-2 control-label">Health</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_health" name="beast_health" value="<?php echo $beast_health; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="beast_strength" class="col-sm-2 control-label">Strength</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_strength" name="beast_strength" value="<?php echo $beast_strength; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="beast_defence" class="col-sm-2 control-label">Defence</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_defence" name="beast_defence" value="<?php echo $beast_defence; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="beast_speed" class="col-sm-2 control-label">Speed</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_speed" name="beast_speed" value="<?php echo $beast_speed; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="beast_luck" class="col-sm-2 control-label">Luck</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="beast_luck" name="beast_luck" value="<?php echo $beast_luck; ?>">
                </div>
            </div>
<!--
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <input id="submit" name="submit" type="submit" value="Let's roll" class="btn btn-primary">
                </div>
            </div>
            -->
            <?php
            include("battle.php");
            ?>
        </form>
    </div>
    <footer style="margin-top: 250px;"></footer>
</body>
</html>