<?php

require_once('character.php');

class Hero extends Character
{
    private $strike_twice = 0;
    private $magic_shield = 0;

    private $strike_built       = 0;
    private $magic_shield_built = 0;

    function __construct(
        $name,
        $health,
        $strength,
        $defence,
        $speed,
        $luck,
        $strike_twice,
        $magic_shield)
    {
        parent::__construct($name, $health, $strength, $defence, $speed, $luck);
        $this->strike_twice   = $strike_twice;
        $this->magic_shield = $magic_shield;
    }

    public function strike()
    {
        $damage = $this->strength;
        $this->strike_built++;
        if($this->strike_built > 0 && rand(0,1)) {
            $damage*= 2;
            $this->strike_actions[] = 'use strike twice';
            // stretch next luck
            $this->strike_built = 0 - ( 100/$this->strike_twice - $this->strike_built );
        }
        if($this->strike_built >= 100/$this->strike_twice) {
            // reset counter on each set of hits per luck
            $this->strike_built = 0;
        }
        return $damage;
    }

    public function extra_defence($hit_damage)
    {
        $this->magic_shield_built++;
        if($this->magic_shield_built > 0 && rand(0,1)) {
            $hit_damage/= 2;
            $this->defence_actions[] = 'use magic shield';
            // stretch next luck
            $this->magic_shield_built = 0 - ( 100/$this->magic_shield - $this->magic_shield_built );
        }
        if($this->magic_shield_built >= 100/$this->magic_shield) {
            // reset counter on each set of hits per luck
            $this->magic_shield_built = 0;
        }
        return $hit_damage;
    }
}

?>